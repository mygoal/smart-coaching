package com.smart_coaching.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.smart_coaching.controller.Const;
@Entity
@Table(name=Const.TABLE_COURSE)
public class CourseResources {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name=Const.COL_RESOURCE_ID)
	private int resource_id;
	
	@Column(name=Const.COL_RESOURCE_NAME)
	private String resource_name;
	
	@Column(name=Const.COL_RESOURCE_TYPE)
	private int resource_type;
	
	@Column(name=Const.COL_COURCE_ID)
	private int course_id;
	
	@Column(name = Const.COL_INSTRUCTOR_ID)
	private int instructor_id;
	
	@Column(name=Const.COL_RESOURCE_LOCATION)
	private String resource_location;
	
	@Column(name=Const.COL_IS_ACTIVE)
	private boolean is_active;
	
	@Column(name = Const.COL_UPLOADED_DATE)
	private Date uploaded_on;
	
	public int getResource_id() {
		return resource_id;
	}
	public void setResource_id(int resource_id) {
		this.resource_id = resource_id;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public int getResource_type() {
		return resource_type;
	}
	public void setResource_type(int resource_type) {
		this.resource_type = resource_type;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public int getInstructor_id() {
		return instructor_id;
	}
	public void setInstructor_id(int instructor_id) {
		this.instructor_id = instructor_id;
	}
	public String getResource_location() {
		return resource_location;
	}
	public void setResource_location(String resource_location) {
		this.resource_location = resource_location;
	}
	public boolean isIs_active() {
		return is_active;
	}
	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}
	public Date getUploaded_on() {
		return uploaded_on;
	}
	public void setUploaded_on(Date uploaded_on) {
		this.uploaded_on = uploaded_on;
	}
	
	
	
	
}
