package com.smart_coaching.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="user_details")
public class Instructor extends UserDetails {

	private List<Subject> subjects;

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
	
	public void addSubject(Subject sub) {
		if(this.subjects==null) subjects = new ArrayList<Subject>();
		subjects.add(sub);
	}
	
}
