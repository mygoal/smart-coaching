package com.smart_coaching.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.smart_coaching.controller.Const;

@Entity(name = Const.TABLE_COURSE)
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name=Const.COL_COURCE_ID)
	private int course_id;
	
	@Column(name = Const.COL_COURCE_NAME)
	private String cource_name;
	
	@Column(name = Const.COL_SUBJECT_ID)
	private int subject_id;
	
	@Column(name=Const.COL_DURATION)
	private int duration;
	
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public String getCource_name() {
		return cource_name;
	}
	public void setCource_name(String cource_name) {
		this.cource_name = cource_name;
	}
	public int getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
}
