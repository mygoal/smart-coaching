package com.smart_coaching.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.smart_coaching.controller.Const;

@Entity
@Table(name=Const.TABLE_USER_DETAILS)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UserDetails {
	
	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int user_id;
	
	@Column(name="role_id")
	private int role_id;
	
//	@ManyToOne(cascade = CascadeType.ALL) // TODO : Need to change cascade
//	@JoinColumn(name="role_id")
//	private UserRole userRole;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="middle_name")
	private String middle_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="address")
	private String address;
	
	@Column(name="qualification_id")
	private int highest_qualification_id;
	
	@Column(name = "branch_id")
	private int branch_id;
	
//	@Column(name = "added_on")
//	private Date added_on;
	
	@Column(name ="added_by")
	private int added_by;
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getHighest_qualification_id() {
		return highest_qualification_id;
	}
	public void setHighest_qualification_id(int highest_qualification_id) {
		this.highest_qualification_id = highest_qualification_id;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	/*
	 * public Date getAdded_on() { return added_on; } public void setAdded_on(Date
	 * added_on) { this.added_on = added_on; }
	 */
	public int getAdded_by() {
		return added_by;
	}
	public void setAdded_by(int added_by) {
		this.added_by = added_by;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	/*
	 * public UserRole getUserRole() { return userRole; } public void
	 * setUserRole(UserRole userRole) { this.userRole = userRole; }
	 */
	
	
	
	
	
}
