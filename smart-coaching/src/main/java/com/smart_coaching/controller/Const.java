package com.smart_coaching.controller;

public interface Const {

	String TABLE_USER_DETAILS = "user_details";
	String TABLE_SUBJECT="subject";
	String TABLE_COURSE="course";
	String TABLE_COURSE_RESOURCES="course_resources";
	
	int ROLE_INSTRUCTOR=1;
	int ROLE_STUDENT = 2;
	
// ---------  Error Messages --------	
	String SUCCESS_OK = "Student not found -";
	String SUCCESS_STUDENT_UPDATED="Student updated successfully.";
	String SUCCESS_STUDENT_SAVED="Student saved successfully.";
	String SUCCESS_STUDENT_DELETED="Student deleted successfully.";
		
		
		
// ---------  Error Messages --------	
	String ERROR_STUDENT_NOT_FOUND = "Student not found -";
	String ERROR_INSTRUCTOR_NOT_FOUND = "Instructor not found -";
	
	
//	----------------- Column Names -----------------------
	
	/* ------------- Resource Table ----------------*/
	final String COL_RESOURCE_ID="resource_id";
	final String COL_RESOURCE_NAME ="resource_name";
	final String COL_RESOURCE_TYPE ="resource_type";
	final String COL_COURCE_ID ="course_id";
	final String COL_INSTRUCTOR_ID="instructor_id";
	final String COL_RESOURCE_LOCATION ="resource_location";
	final String COL_IS_ACTIVE="is_active";
	final String COL_UPLOADED_DATE="uploaded_on";

	/* ------------- course Table ----------------*/
	final String COL_COURSE_ID="course_id";
	final String COL_COURCE_NAME="cource_name";
	final String COL_SUNBJECT_ID ="subject_id";
	final String COL_DURATION ="duration";
	
	/* ------------- Subject Table ----------------*/
	final String COL_SUBJECT_ID ="subject_id";
	final String COL_SUBJECT_NAME="subject_name";
	
	




}
