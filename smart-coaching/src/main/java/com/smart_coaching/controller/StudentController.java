package com.smart_coaching.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smart_coaching.exception_handler.ExceptionStudentNotFound;
import com.smart_coaching.model.UserDetails;
import com.smart_coaching.services.StudentServices;

@RestController
@RequestMapping("/api")
public class StudentController {
	
	@Autowired
	private StudentServices studentServices;
	
	@GetMapping("/student/{stud_id}")
	public UserDetails getStudentDetails(@PathVariable("stud_id") int stud_id) {
		if(stud_id <= 0) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND+ stud_id);
		UserDetails user = studentServices.getStudent(stud_id);
		if(user== null) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND+ stud_id);
		return user;
	}
	@PostMapping("/student")
	public UserDetails saveStudentDetails(@RequestBody UserDetails student) {
		if(student.getUser_id() < 0)student.setUser_id(0);
		studentServices.saveStudent(student);
		return student;
	}
	
	@PutMapping("/student/{student_id}")
	public UserDetails updateStudentDetails(@PathVariable("student_id") int student_id, @RequestBody UserDetails student) {
		if(student_id <= 0) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND+ student_id);
		studentServices.saveStudent(student);
		student = new UserDetails();
		return student;
	}
	
	@DeleteMapping("student/{student_id}")
	public boolean deleteStudent(@PathVariable("student_id") int student_id) {
		if(student_id <0) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND + student_id);
		else {
			UserDetails user = studentServices.getStudent(student_id);
			if(user== null) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND+student_id);
		}
		return studentServices.deleteStudent(student_id);
	}
	
	
}
