package com.smart_coaching.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smart_coaching.exception_handler.ExceptionInstructorNotFound;
import com.smart_coaching.exception_handler.ExceptionStudentNotFound;
import com.smart_coaching.model.Instructor;
import com.smart_coaching.model.UserDetails;
import com.smart_coaching.services.InstructorServices;

@RestController
@RequestMapping("/api")
public class InstructorController {
	
	@Autowired
	InstructorServices instructorServices; 

	@GetMapping(value = {"/instructor"})
	public List<UserDetails> getInstructor() {
		return instructorServices.getInstructor();
	}
	
	@GetMapping(value = "/instructor/{instructor_id}")
	public UserDetails getInstructor(@PathVariable("instructor_id") int instructor_id) {
		if(instructor_id<=0) throw new ExceptionInstructorNotFound(Const.ERROR_INSTRUCTOR_NOT_FOUND + instructor_id);
		return instructorServices.getInstructor(instructor_id);
	}
	
	@PostMapping(value="/instructor")
	public UserDetails saveInstructur(@RequestBody UserDetails instructor) {
		if(instructor.getUser_id() < 0)instructor.setUser_id(0);
		try{
			instructorServices.saveInstructor(instructor);
		}catch(Exception e) {System.out.println("Save Instructor = "+e);}
		return instructor;
	}
	
	@DeleteMapping(value = "/instructor/{instructor_id}")
	public boolean deleteInstructor(@PathVariable("instructor_id") int instructor_id) {
		if(instructor_id<=0) throw new ExceptionInstructorNotFound(Const.ERROR_INSTRUCTOR_NOT_FOUND + instructor_id);
		else {
			UserDetails user = instructorServices.getInstructor(instructor_id);
			if(user == null) throw new ExceptionInstructorNotFound(Const.ERROR_INSTRUCTOR_NOT_FOUND+ instructor_id);
		}
		return instructorServices.deleteInstructor(instructor_id);
		
	}
	@PutMapping("/instructor/{instructor_id}")
	public UserDetails updateStudentDetails(@PathVariable("instructor_id") int student_id, @RequestBody UserDetails instructor) {
		if(student_id <= 0) throw new ExceptionStudentNotFound(Const.ERROR_STUDENT_NOT_FOUND+ student_id);
		instructorServices.saveInstructor(instructor);
		instructor = new UserDetails();
		return instructor;
	}
	
	
}
