package com.smart_coaching.exception_handler;

public class ExceptionInstructorNotFound extends RuntimeException	{
	private static final long serialVersionUID = 1L;

	public ExceptionInstructorNotFound(String message, Throwable cause) {
		super(message, cause);
	}

	public ExceptionInstructorNotFound(String message) {
		super(message);
	}

	public ExceptionInstructorNotFound(Throwable cause) {
		super(cause);
	}

}
