package com.smart_coaching.exception_handler;

public class ExceptionStudentNotFound extends RuntimeException	{
	private static final long serialVersionUID = 1L;

	public ExceptionStudentNotFound(String message, Throwable cause) {
		super(message, cause);
	}

	public ExceptionStudentNotFound(String message) {
		super(message);
	}

	public ExceptionStudentNotFound(Throwable cause) {
		super(cause);
	}

}
