package com.smart_coaching.services;

import java.util.List;

import com.smart_coaching.model.Instructor;
import com.smart_coaching.model.UserDetails;

public interface InstructorServices {

	public UserDetails getInstructor(int instructor_id);
	
	public List<UserDetails> getInstructor();
	
	public void saveInstructor(UserDetails instructor);
	
	public boolean deleteInstructor(int instructor_id);
	
	public UserDetails updateInstructor(UserDetails instructor);

	
}
