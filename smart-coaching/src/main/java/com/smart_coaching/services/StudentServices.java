package com.smart_coaching.services;

import java.util.List;

import com.smart_coaching.model.UserDetails;

public interface StudentServices {
	public void saveStudent(UserDetails student);
	public UserDetails getStudent(int stud_id);
	
	public UserDetails updateStudentDetails(UserDetails student);
	public List<UserDetails> getAllStudents();
	public boolean deleteStudent(int user_id);
	
}
