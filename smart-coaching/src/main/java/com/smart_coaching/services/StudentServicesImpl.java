package com.smart_coaching.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart_coaching.DAO.UserDao;
import com.smart_coaching.controller.Const;
import com.smart_coaching.model.UserDetails;

@Service
public class StudentServicesImpl implements StudentServices {

	@Autowired
	UserDao userDao;
	
	@Override
	@Transactional
	public void saveStudent(UserDetails student) {
		student.setRole_id(Const.ROLE_STUDENT);
		userDao.saveUserDetails(student);
	}
	@Override
	@Transactional
	public UserDetails getStudent(int stud_id) {
		return userDao.getUserDetails(stud_id);
	}
	@Override
	@Transactional
	public UserDetails updateStudentDetails(UserDetails student) {
		return userDao.updateUserDetails(student);
	}
	@Override
	@Transactional
	public List<UserDetails> getAllStudents() {
		return userDao.retrieveAllUserDetails(Const.ROLE_STUDENT);
	}
	@Override
	@Transactional
	public boolean deleteStudent(int user_id) {
		return userDao.deleteUserDetails(user_id);
	}
}
