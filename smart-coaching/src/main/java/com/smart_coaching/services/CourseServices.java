package com.smart_coaching.services;

import java.util.List;

import com.smart_coaching.model.Course;

public interface CourseServices {

	public Course createCourse(Course course);
	
	public Course getCourseDetails(int course_id);
	
	public List<Course> getCourseList();
	
	
}
