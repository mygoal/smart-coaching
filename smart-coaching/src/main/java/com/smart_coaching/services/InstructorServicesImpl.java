package com.smart_coaching.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart_coaching.DAO.UserDao;
import com.smart_coaching.controller.Const;
import com.smart_coaching.model.Instructor;
import com.smart_coaching.model.UserDetails;

@Service
public class InstructorServicesImpl implements InstructorServices {

	@Autowired
	UserDao userDao;
	
	@Override
	@Transactional
	public UserDetails getInstructor(int instructor_id) {
		return userDao.getUserDetails(instructor_id);
	}

	@Override
	@Transactional
	public List<UserDetails> getInstructor() {
		int role = Const.ROLE_INSTRUCTOR;
		return userDao.retrieveAllUserDetails(role);
	}

	@Override
	@Transactional
	public boolean deleteInstructor(int instructor_id) {
		return userDao.deleteUserDetails(instructor_id);
	}

	@Override
	@Transactional
	public UserDetails updateInstructor(UserDetails instructor) {
		return userDao.updateUserDetails(instructor);
	}

	@Override
	@Transactional
	public void saveInstructor(UserDetails instructor) {
		instructor.setRole_id(Const.ROLE_INSTRUCTOR);
		 userDao.saveUserDetails(instructor);;
	}
	

}
