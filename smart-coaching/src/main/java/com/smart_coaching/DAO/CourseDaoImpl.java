package com.smart_coaching.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import com.smart_coaching.model.Course;

public class CourseDaoImpl implements CourseDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public Course createCourse(Course course) {
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(course);
		return course;
	}

	@Override
	public Course getCourseDetails(int course_id) {
		Session session=sessionFactory.getCurrentSession();
		Course course=session.get(Course.class, course_id);
		return course;
	}

	@Override
	public List<Course> getCourseList() {
		Session session=sessionFactory.getCurrentSession();
		String SQL="from user_details where role_id=:rollid";
		Query query= session.createQuery(SQL);
		List<Course> courses = query.getResultList();
		return courses;
	}

}
