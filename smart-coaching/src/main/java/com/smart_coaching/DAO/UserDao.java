package com.smart_coaching.DAO;

import java.util.List;

import com.smart_coaching.model.Instructor;
import com.smart_coaching.model.UserDetails;

public interface UserDao {
	public void saveUserDetails(UserDetails student);
	
	public UserDetails getUserDetails(int stud_id) ;
	public List<UserDetails> retrieveAllUserDetails(int roll);
	
	public UserDetails updateUserDetails(UserDetails userDetails); 
	public boolean deleteUserDetails(int user_id);

}
