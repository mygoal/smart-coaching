package com.smart_coaching.DAO;

import org.hibernate.query.Query;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.smart_coaching.model.Instructor;
import com.smart_coaching.model.UserDetails;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void saveUserDetails(UserDetails student) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.saveOrUpdate(student);
		}catch(Exception e) {
			System.out.println("What is Exception = "+e);
		}
	}
	
	@Override
	public UserDetails getUserDetails(int stud_id) {
		Session session = sessionFactory.getCurrentSession();
		UserDetails userDetails = session.get(UserDetails.class, stud_id);
		return userDetails;
	}

	@Override
	public List<UserDetails> retrieveAllUserDetails(int role_id) {
		Session session = sessionFactory.getCurrentSession();
		String SQL="from user_details where role_id=:rollid";
		Query query= session.createQuery(SQL);
		query.setParameter("rollid", role_id);
		List<UserDetails> users = query.getResultList();
		return users;
	}

	@Override
	public UserDetails updateUserDetails(UserDetails userDetails) {
		UserDetails user = getUserDetails(userDetails.getUser_id());
		if(user == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(userDetails);
		return userDetails;
	}

	@Override
	public boolean deleteUserDetails(int user_id) {
		boolean ret = false;
		Session session = sessionFactory.getCurrentSession();
		UserDetails user = session.get(UserDetails.class, user_id);
		session.delete(user);
		
//		try {
//			String SQL ="delete from user_details where user_id=:uid";
//			Query query= session.createQuery(SQL);
//			query.setParameter("uid", user_id);
//			int cnt = query.executeUpdate(); if(cnt > 0) ret =true;
//		}catch(Exception e) {
//			System.out.println("Delete UserDetails = "+e);
//		}
		return ret;
	}

	
}
