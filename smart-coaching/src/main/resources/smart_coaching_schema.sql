create schema smart_coaching;
USE smart_coaching;

DROP TABLE IF EXISTS `role_master`;
CREATE TABLE role_master(
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(45),
  PRIMARY KEY(`role_id`)
);

INSERT INTO `role_master` (`role_id`, `role_name`) values(1, 'Instructor');
INSERT INTO `role_master` (`role_id`, `role_name`) values(2, 'Student');

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE user_details(
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `address` varchar(50),
  `qualification_id` int(11) DEFAULT 0,
  `branch_id` int(11) DEFAULT 0,
  `added_on` datetime DEFAULT null,
  `added_by` int(11) NOT NULL,
  primary key (`user_id`),

  CONSTRAINT `FK_role_id` FOREIGN KEY (`role_id`) REFERENCES `role_master` (`role_id`)
);

CREATE TABLE subject (
`subject_id` int(11) NOT NULL AUTO_INCREMENT,
`subject_name` varchar(45) NOT NULL,
PRIMARY KEY(`subject_id`)
);

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `cource_name` varchar(50) NOT NULL,
  `subject_id` INT(11) NOT NULL,
  `duration` bigint(21),
  PRIMARY KEY(`course_id`),
  CONSTRAINT `FK_subject_id` FOREIGN KEY(`subject_id`) REFERENCES `subject`(`subject_id`)
)

DROP TABLE IF EXISTS `subject`;
CREATE TABLE subject (
`subject_id` int(11) NOT NULL AUTO_INCREMENT,
`subject_name` varchar(45) NOT NULL,
PRIMARY KEY(`subject_id`)
);


DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `cource_name` varchar(50) NOT NULL,
  `subject_id` INT(11) NOT NULL,
  `duration` bigint(21),
  PRIMARY KEY(`course_id`),
  CONSTRAINT `FK_subject_id` FOREIGN KEY(`subject_id`) REFERENCES `subject`(`subject_id`)
);


DROP TABLE IF EXISTS `course_instructor_map`;
CREATE TABLE course_instructor_map(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instructor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  CONSTRAINT `FK_instructor_id` FOREIGN KEY(`instructor_id`) REFERENCES `user_details` (`user_id`),
  CONSTRAINT `FK_course_id` FOREIGN KEY(`course_id`) REFERENCES `course`(`course_id`),
  PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `course_resources`;
CREATE TABLE `course_resources`(
  `resource_id` int(11) not null auto_increment,
  `resource_name` varchar(45) not null,
  `resource_type` int(11) NOT NULL, -- 1: Webinar, 2: video
  `course_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `resource_location` text NOT NULL,
  `is_active` int(11) default 1, -- 0: inactive(deleted), 1: active
  `uploaded_on` datetime DEFAULT null,
  CONSTRAINT `FK_instructor_id_res` FOREIGN KEY(`instructor_id`) REFERENCES `user_details` (`user_id`),
  CONSTRAINT `FK_course_id_res` FOREIGN KEY(`course_id`) REFERENCES `course`(`course_id`),
  primary key(`resource_id`)
);



-- one instructor can have multiple courses
-- 
